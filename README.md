# hi-devops

Résultat du tutoriel : [https://medium.com/google-cloud/automatically-deploy-to-google-app-engine-with-gitlab-ci-d1c7237cbe11](https://medium.com/google-cloud/automatically-deploy-to-google-app-engine-with-gitlab-ci-d1c7237cbe11)

Lors d'un commit sur la branche "staging", l'application est déployée sur [https://staging-dot-hi-devops.appspot.com/](https://staging-dot-hi-devops.appspot.com/)

Lors d'un commit sur le master, l'application est déployée sur [https://hi-devops.appspot.com/](https://hi-devops.appspot.com/)

![branches](README/branches.png)

# configuration gcloud (https://cloud.google.com/)

création d'un projet App Engine. Cela crée notamment les deux storage buckets :
* hi-devops.appspot.com
* staging.hi-devops.appspot.com

création d'un service account (que j'ai nommé "gitlab-user")

![service account](README/service-account-roles.png)

ajout de plusieurs rôles avant que ça fonctionne (lesquels sont nécessaires ?)

ajout du service account en tant que membre des deux storage buckets

Activation de l'API cloud-build

# configuration gitlab

Création des variables d'environnement dans -/settings/ci_cd
* PROJECT_ID
* SERVICE_ACCOUNT

# documentation

[https://docs.gitlab.com/ee/workflow/gitlab_flow.html](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)

[https://docs.gitlab.com/ee/ci/environments.html](https://docs.gitlab.com/ee/ci/environments.html)
