const express = require('express');
const path = require('path');
const app = express();

app.get('/', (req, res) => {
  res.status(200).send('Hi DevOps - Seb');
});

const server = app.listen(process.env.PORT || 8080, () => {
  const port = server.address().port;
  console.log(`App listening on port ${port}`);
});

module.exports = app;